import os
import json
import subprocess
import struct

center_frequencies = {
    0: 2625,
    1: 3575
}

offset_indices = {
    0: [0, 1, 2, 3, 4, 6],
    1: [0, 1, 2, 3, 4, 5]
}

tx_names = {
    0: {
        0: "bes",
        1: "browning",
        2: "fm",
        3: "honors",
        4: "hospital",
        6: "ustar",
    },
    1: {
        0: "browning",
        1: "fm",
        2: "honors",
        3: "hospital",
        4: "smt",
        5: "ustar",
    },
}

tx_power = {
    0: {
        0: 30.0,
        1: 18.5,
        2: 18.5,
        3: 18.5,
        4: 18.5,
        6: 18.5,
    },
    1: {
        0: 15.1,
        1: 15.1,
        2: 15.1,
        3: 15.1,
        4: 15.1,
        5: 15.1,
    },
}

output_data_json = {}

if not os.path.isdir("out_data"):
    os.mkdir("out_data")

for thing in os.listdir('data'):
    in_dir = os.path.join("data", thing)
    if not os.path.isdir(in_dir):
        continue

    output_data_json[thing] = {"2625": {}, "3575": {}}

    out_dir = os.path.join("out_data", thing)
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)

    with open(os.path.join(in_dir, 'data.json'), 'r') as offset_file:
        offsets_json = json.load(offset_file)

    for data_filename, data in offsets_json.items():
        print(thing, data_filename)
        center_frequency_index = data["center_frequency_index"]
        offset = data["offset"]
        print(offset, center_frequency_index)

        for i in offset_indices[center_frequency_index]:
            out_filename = os.path.join(out_dir, data_filename + "." + str(i))
            subprocess.run(["./rx3.py", "--center-frequency-index",
                            str(center_frequency_index),
                            "--extra-offset-hz", str(offset),
                            "--offset-frequency-index", str(i),
                            "--in-filename",
                            os.path.join(in_dir, data_filename),
                            "--out-filename", out_filename])
            with open(out_filename, "rb") as out_file:
                all_magnitudes = []
                for j in range(1010):
                    out_data = out_file.read(8)
                while True:
                    out_data = out_file.read(8)
                    if len(out_data) != 8:
                        break
                    phase, magnitude = struct.unpack("<ff", out_data)
                    all_magnitudes.append(magnitude)
                tx_name = tx_names[center_frequency_index][i]
                print(f"tx: {tx_name} rx: {thing}")
                center_frequency = center_frequencies[center_frequency_index]
                center_frequency_str = str(center_frequency)
                output_data_json[thing][center_frequency_str][tx_name] = {}
                all_magnitudes.sort()
                for j in [10, 20, 50, 80, 90]:
                    index = int((len(all_magnitudes)-1)*(j/100))
                    print(f"{j}%: {all_magnitudes[index]}")
                    output_data_json[thing][center_frequency_str][tx_name][
                        str(j)] = {
                            "magnitude": all_magnitudes[index],
                            "path_loss": (tx_power[center_frequency_index][i]
                                          - all_magnitudes[index])}

with open("out_data/data.json", "w") as out_data_file:
    json.dump(output_data_json, out_data_file)
