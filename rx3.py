#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Receiver
# GNU Radio version: 3.8.1.0

from gnuradio import analog
from gnuradio import blocks
import pmt
from gnuradio import filter
from gnuradio.filter import firdes
from gnuradio import gr
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation


class rx3(gr.top_block):

    def __init__(self, center_frequency_index=0, extra_offset_hz=0, in_filename="data.dat", offset_frequency_index=0, out_filename="/dev/null", pll_loop_bw_hz=50, pll_max_freq_hz=1000, pll_min_freq_hz=-1000, pre_pll_bw=100):
        gr.top_block.__init__(self, "Receiver")

        ##################################################
        # Parameters
        ##################################################
        self.center_frequency_index = center_frequency_index
        self.extra_offset_hz = extra_offset_hz
        self.in_filename = in_filename
        self.offset_frequency_index = offset_frequency_index
        self.out_filename = out_filename
        self.pll_loop_bw_hz = pll_loop_bw_hz
        self.pll_max_freq_hz = pll_max_freq_hz
        self.pll_min_freq_hz = pll_min_freq_hz
        self.pre_pll_bw = pre_pll_bw

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 512000
        self.samp_rate_1_5 = samp_rate_1_5 = samp_rate/(2*4*8)
        self.pll_samp_rate = pll_samp_rate = samp_rate_1_5
        self.pll_min_freq = pll_min_freq = 2*3.14159/pll_samp_rate*pll_min_freq_hz
        self.pll_max_freq = pll_max_freq = 2*3.14159/pll_samp_rate*pll_max_freq_hz
        self.pll_loop_bw = pll_loop_bw = 2*3.14159/pll_samp_rate*pll_loop_bw_hz
        self.offset_frequencies = offset_frequencies = [_*5000 for _ in [2,3,5,7,11,13,17,19,23]]
        self.dbm_offsets = dbm_offsets = [-55.2,-46.55]
        self.center_frequencies_mhz = center_frequencies_mhz = [2625,3575]

        ##################################################
        # Blocks
        ##################################################
        self.rational_resampler_xxx_1 = filter.rational_resampler_ccc(
                interpolation=1,
                decimation=8,
                taps=None,
                fractional_bw=None)
        self.rational_resampler_xxx_0 = filter.rational_resampler_ccc(
                interpolation=1,
                decimation=4,
                taps=None,
                fractional_bw=None)
        self.low_pass_filter_0_0 = filter.fir_filter_ccf(
            1,
            firdes.low_pass(
                1,
                samp_rate_1_5,
                pre_pll_bw,
                pre_pll_bw/2,
                firdes.WIN_HAMMING,
                6.76))
        self.low_pass_filter_0 = filter.fir_filter_ccf(
            2,
            firdes.low_pass(
                1,
                samp_rate,
                samp_rate/4,
                samp_rate/8,
                firdes.WIN_HAMMING,
                6.76))
        self.blocks_streams_to_vector_0 = blocks.streams_to_vector(gr.sizeof_float*1, 2)
        self.blocks_nlog10_ff_0 = blocks.nlog10_ff(20, 1, dbm_offsets[center_frequency_index])
        self.blocks_multiply_xx_0 = blocks.multiply_vcc(1)
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_ff(pll_samp_rate/(2*3.14159))
        self.blocks_moving_average_xx_0 = blocks.moving_average_cc(1000, 1e-3, 4000, 1)
        self.blocks_file_source_0 = blocks.file_source(gr.sizeof_gr_complex*1, in_filename, False, 0, 0)
        self.blocks_file_source_0.set_begin_tag(pmt.PMT_NIL)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_float*2, out_filename, False)
        self.blocks_file_sink_0.set_unbuffered(False)
        self.blocks_complex_to_mag_0 = blocks.complex_to_mag(1)
        self.analog_sig_source_x_0 = analog.sig_source_c(samp_rate, analog.GR_COS_WAVE, -offset_frequencies[offset_frequency_index]-extra_offset_hz, 1, 0, 0)
        self.analog_pll_freqdet_cf_0 = analog.pll_freqdet_cf(pll_loop_bw, pll_max_freq, pll_min_freq)
        self.analog_pll_carriertracking_cc_0 = analog.pll_carriertracking_cc(pll_loop_bw, pll_max_freq, pll_min_freq)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_pll_carriertracking_cc_0, 0), (self.blocks_moving_average_xx_0, 0))
        self.connect((self.analog_pll_freqdet_cf_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_multiply_xx_0, 1))
        self.connect((self.blocks_complex_to_mag_0, 0), (self.blocks_nlog10_ff_0, 0))
        self.connect((self.blocks_file_source_0, 0), (self.blocks_multiply_xx_0, 0))
        self.connect((self.blocks_moving_average_xx_0, 0), (self.blocks_complex_to_mag_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_streams_to_vector_0, 0))
        self.connect((self.blocks_multiply_xx_0, 0), (self.low_pass_filter_0, 0))
        self.connect((self.blocks_nlog10_ff_0, 0), (self.blocks_streams_to_vector_0, 1))
        self.connect((self.blocks_streams_to_vector_0, 0), (self.blocks_file_sink_0, 0))
        self.connect((self.low_pass_filter_0, 0), (self.rational_resampler_xxx_0, 0))
        self.connect((self.low_pass_filter_0_0, 0), (self.analog_pll_carriertracking_cc_0, 0))
        self.connect((self.low_pass_filter_0_0, 0), (self.analog_pll_freqdet_cf_0, 0))
        self.connect((self.rational_resampler_xxx_0, 0), (self.rational_resampler_xxx_1, 0))
        self.connect((self.rational_resampler_xxx_1, 0), (self.low_pass_filter_0_0, 0))


    def get_center_frequency_index(self):
        return self.center_frequency_index

    def set_center_frequency_index(self, center_frequency_index):
        self.center_frequency_index = center_frequency_index

    def get_extra_offset_hz(self):
        return self.extra_offset_hz

    def set_extra_offset_hz(self, extra_offset_hz):
        self.extra_offset_hz = extra_offset_hz
        self.analog_sig_source_x_0.set_frequency(-self.offset_frequencies[self.offset_frequency_index]-self.extra_offset_hz)

    def get_in_filename(self):
        return self.in_filename

    def set_in_filename(self, in_filename):
        self.in_filename = in_filename
        self.blocks_file_source_0.open(self.in_filename, False)

    def get_offset_frequency_index(self):
        return self.offset_frequency_index

    def set_offset_frequency_index(self, offset_frequency_index):
        self.offset_frequency_index = offset_frequency_index
        self.analog_sig_source_x_0.set_frequency(-self.offset_frequencies[self.offset_frequency_index]-self.extra_offset_hz)

    def get_out_filename(self):
        return self.out_filename

    def set_out_filename(self, out_filename):
        self.out_filename = out_filename
        self.blocks_file_sink_0.open(self.out_filename)

    def get_pll_loop_bw_hz(self):
        return self.pll_loop_bw_hz

    def set_pll_loop_bw_hz(self, pll_loop_bw_hz):
        self.pll_loop_bw_hz = pll_loop_bw_hz
        self.set_pll_loop_bw(2*3.14159/self.pll_samp_rate*self.pll_loop_bw_hz)

    def get_pll_max_freq_hz(self):
        return self.pll_max_freq_hz

    def set_pll_max_freq_hz(self, pll_max_freq_hz):
        self.pll_max_freq_hz = pll_max_freq_hz
        self.set_pll_max_freq(2*3.14159/self.pll_samp_rate*self.pll_max_freq_hz)

    def get_pll_min_freq_hz(self):
        return self.pll_min_freq_hz

    def set_pll_min_freq_hz(self, pll_min_freq_hz):
        self.pll_min_freq_hz = pll_min_freq_hz
        self.set_pll_min_freq(2*3.14159/self.pll_samp_rate*self.pll_min_freq_hz)

    def get_pre_pll_bw(self):
        return self.pre_pll_bw

    def set_pre_pll_bw(self, pre_pll_bw):
        self.pre_pll_bw = pre_pll_bw
        self.low_pass_filter_0_0.set_taps(firdes.low_pass(1, self.samp_rate_1_5, self.pre_pll_bw, self.pre_pll_bw/2, firdes.WIN_HAMMING, 6.76))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_samp_rate_1_5(self.samp_rate/(2*4*8))
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)
        self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, self.samp_rate/4, self.samp_rate/8, firdes.WIN_HAMMING, 6.76))

    def get_samp_rate_1_5(self):
        return self.samp_rate_1_5

    def set_samp_rate_1_5(self, samp_rate_1_5):
        self.samp_rate_1_5 = samp_rate_1_5
        self.set_pll_samp_rate(self.samp_rate_1_5)
        self.low_pass_filter_0_0.set_taps(firdes.low_pass(1, self.samp_rate_1_5, self.pre_pll_bw, self.pre_pll_bw/2, firdes.WIN_HAMMING, 6.76))

    def get_pll_samp_rate(self):
        return self.pll_samp_rate

    def set_pll_samp_rate(self, pll_samp_rate):
        self.pll_samp_rate = pll_samp_rate
        self.set_pll_loop_bw(2*3.14159/self.pll_samp_rate*self.pll_loop_bw_hz)
        self.set_pll_max_freq(2*3.14159/self.pll_samp_rate*self.pll_max_freq_hz)
        self.set_pll_min_freq(2*3.14159/self.pll_samp_rate*self.pll_min_freq_hz)
        self.blocks_multiply_const_vxx_0.set_k(self.pll_samp_rate/(2*3.14159))

    def get_pll_min_freq(self):
        return self.pll_min_freq

    def set_pll_min_freq(self, pll_min_freq):
        self.pll_min_freq = pll_min_freq
        self.analog_pll_carriertracking_cc_0.set_min_freq(self.pll_min_freq)
        self.analog_pll_freqdet_cf_0.set_min_freq(self.pll_min_freq)

    def get_pll_max_freq(self):
        return self.pll_max_freq

    def set_pll_max_freq(self, pll_max_freq):
        self.pll_max_freq = pll_max_freq
        self.analog_pll_carriertracking_cc_0.set_max_freq(self.pll_max_freq)
        self.analog_pll_freqdet_cf_0.set_max_freq(self.pll_max_freq)

    def get_pll_loop_bw(self):
        return self.pll_loop_bw

    def set_pll_loop_bw(self, pll_loop_bw):
        self.pll_loop_bw = pll_loop_bw
        self.analog_pll_carriertracking_cc_0.set_loop_bandwidth(self.pll_loop_bw)
        self.analog_pll_freqdet_cf_0.set_loop_bandwidth(self.pll_loop_bw)

    def get_offset_frequencies(self):
        return self.offset_frequencies

    def set_offset_frequencies(self, offset_frequencies):
        self.offset_frequencies = offset_frequencies
        self.analog_sig_source_x_0.set_frequency(-self.offset_frequencies[self.offset_frequency_index]-self.extra_offset_hz)

    def get_dbm_offsets(self):
        return self.dbm_offsets

    def set_dbm_offsets(self, dbm_offsets):
        self.dbm_offsets = dbm_offsets

    def get_center_frequencies_mhz(self):
        return self.center_frequencies_mhz

    def set_center_frequencies_mhz(self, center_frequencies_mhz):
        self.center_frequencies_mhz = center_frequencies_mhz




def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "--center-frequency-index", dest="center_frequency_index", type=intx, default=0,
        help="Set center_frequency_index [default=%(default)r]")
    parser.add_argument(
        "--extra-offset-hz", dest="extra_offset_hz", type=eng_float, default="0.0",
        help="Set extra_offset_hz [default=%(default)r]")
    parser.add_argument(
        "--in-filename", dest="in_filename", type=str, default="data.dat",
        help="Set in_filename [default=%(default)r]")
    parser.add_argument(
        "--offset-frequency-index", dest="offset_frequency_index", type=intx, default=0,
        help="Set offset_frequency_index [default=%(default)r]")
    parser.add_argument(
        "--out-filename", dest="out_filename", type=str, default="/dev/null",
        help="Set out_filename [default=%(default)r]")
    parser.add_argument(
        "--pll-loop-bw-hz", dest="pll_loop_bw_hz", type=eng_float, default="50.0",
        help="Set pll_loop_bw_hz [default=%(default)r]")
    parser.add_argument(
        "--pll-max-freq-hz", dest="pll_max_freq_hz", type=eng_float, default="1.0k",
        help="Set pll_max_freq_hz [default=%(default)r]")
    parser.add_argument(
        "--pll-min-freq-hz", dest="pll_min_freq_hz", type=eng_float, default="-1.0k",
        help="Set pll_min_freq_hz [default=%(default)r]")
    parser.add_argument(
        "--pre-pll-bw", dest="pre_pll_bw", type=intx, default=100,
        help="Set pre_pll_bw [default=%(default)r]")
    return parser


def main(top_block_cls=rx3, options=None):
    if options is None:
        options = argument_parser().parse_args()
    tb = top_block_cls(center_frequency_index=options.center_frequency_index, extra_offset_hz=options.extra_offset_hz, in_filename=options.in_filename, offset_frequency_index=options.offset_frequency_index, out_filename=options.out_filename, pll_loop_bw_hz=options.pll_loop_bw_hz, pll_max_freq_hz=options.pll_max_freq_hz, pll_min_freq_hz=options.pll_min_freq_hz, pre_pll_bw=options.pre_pll_bw)

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()

    tb.wait()


if __name__ == '__main__':
    main()
