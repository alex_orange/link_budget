import math

def hav(theta):
    return math.sin(theta/2.0)**2

def ahav(h):
    return 2.0*math.asin(math.sqrt(h))

def haversine(lat1, lng1, lat2, lng2):
    lat1_rad = lat1/180.0 * math.pi
    lng1_rad = lng1/180.0 * math.pi
    lat2_rad = lat2/180.0 * math.pi
    lng2_rad = lng2/180.0 * math.pi

    return (hav(lat2_rad-lat1_rad)
            + math.cos(lat1_rad)*math.cos(lat2_rad)*hav(lng2_rad-lng1_rad))

def haversine_distance(lat1, lng1, lat2, lng2):
    r = 6378e3
    return r*ahav(haversine(lat1, lng1, lat2, lng2))
