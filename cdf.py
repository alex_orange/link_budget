import struct

tx_names = {
    0: {
        0: "bes",
        1: "browning",
        2: "fm",
        3: "honors",
        4: "hospital",
        6: "ustar",
    },
    1: {
        0: "browning",
        1: "fm",
        2: "honors",
        3: "hospital",
        4: "smt",
        5: "ustar",
    },
}



out_filename = "test.dat"

i = 0
center_frequency_index = 0
thing = "bookstore"

center_frequencies = {
    0: 2625,
    1: 3575
}

offset_indices = {
    0: [0, 1, 2, 3, 4, 6],
    1: [0, 1, 2, 3, 4, 5]
}


with open(out_filename, "rb") as out_file:
    all_magnitudes = []
    for j in range(1010):
        out_data = out_file.read(8)
    while True:
        out_data = out_file.read(8)
        if len(out_data) != 8:
            break
        phase, magnitude = struct.unpack("<ff", out_data)
        all_magnitudes.append(magnitude)
    tx_name = tx_names[center_frequency_index][i]
    print(f"tx: {tx_name} rx: {thing}")
    center_frequency = center_frequencies[center_frequency_index]
    center_frequency_str = str(center_frequency)
    output_data_json = {}
    all_magnitudes.sort()
    for j in [10, 20, 50, 80, 90]:
        index = int((len(all_magnitudes)-1)*(j/100))
        print(f"{j}%: {all_magnitudes[index]}")
        output_data_json[
            str(j)] = {
                "magnitude": all_magnitudes[index],}
