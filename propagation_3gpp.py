import math
from dataclasses import dataclass

@dataclass
class AntennaSetup:
    # All numbers in meters

    # h_bs
    base_station_height: float
    # h_ut
    user_equipment_height: float
    # d_2d distance along ground
    distance_2d: float
    # d_3d distance through space, including height
    distance_3d: float
    # W
    average_street_width: float
    # fc (Hz)
    center_frequency: float
    # h
    average_building_height: float = 5.0

c = 3e8

def d_bp(setup):
    return (4.0 * setup.base_station_height * setup.user_equipment_height
            * setup.center_frequency / c)

# Rural macro line of sight
def rma_los(setup):
    def pl1(distance):
        pl1 = (20.0 * math.log10(4.0 * math.pi * distance
                                 * setup.center_frequency / c)
               + min(0.03 * setup.average_building_height**1.72, 10)
                    * math.log10(distance)
               - min(0.044 * setup.average_building_height**1.72, 14.77)
               + 0.002 * math.log10(setup.average_building_height)
                    * distance)
        return pl1

    if setup.distance_2d < 10:
        raise Exception("Not valid less than 10m")
    elif setup.distance_2d > 10e3:
        raise Exception("Not valid over 10km")
    else:
        dbp = d_bp(setup)
        if setup.distance_2d < dbp:
            return pl1(setup.distance_3d), 4.0
        else:
            return pl1(dbp) + 40.0*math.log10(setup.distance_3d / dbp), 6.0

# Rural macro non line of sight
def rma_nlos(setup):
    if setup.distance_2d < 10:
        raise Exception("Not valid less than 10m")
    elif setup.distance_2d > 5e3:
        raise Exception("Not valid over 5km")

    pl_nlos = (161.04 - 7.1 * math.log10(setup.average_street_width)
               + 7.5 * math.log10(setup.average_building_height)
               - (24.37 - 3.7 * (setup.average_building_height /
                                 setup.base_station_height)**2)
                    * math.log10(setup.base_station_height)
               + (43.42 - 3.1 * math.log10(setup.base_station_height))
                    * (math.log10(setup.distance_3d) - 3.0)
               + 20 * math.log10(setup.center_frequency / 1e9)
               - (3.2 * (math.log10(11.75 * setup.user_equipment_height)**2
                         - 4.97)))
    return max(pl_nlos, rma_los(setup)[0]), 8.0

# Urban macro line of sight
def uma_los(setup):
    if setup.distance_2d < 10:
        raise Exception("Not valid less than 10m")
    elif setup.distance_2d > 5e3:
        raise Exception("Not valid over 5km")

    if setup.distance_2d < d_bp(setup):
        return (28.0 + 22.0 * math.log10(setup.distance_3d)
                + 20.0 * math.log10(setup.center_frequency/1e9)), 4.0
    else:
        return (28.0 + 40.0 * math.log10(setup.distance_3d)
                + 20.0 * math.log10(setup.center_frequency/1e9)
                - 9.0 * math.log10(d_bp(setup)**2
                                   + (setup.base_station_height
                                      - setup.user_equipment_height)**2)), 4.0

# Urban macro non line of sight
def uma_nlos(setup):
    if setup.distance_2d < 10:
        raise Exception("Not valid less than 10m")
    elif setup.distance_2d > 5e3:
        raise Exception("Not valid over 5km")

    nlos = (13.54 + 39.08 * math.log10(setup.distance_3d)
            + 20.0 * math.log10(setup.center_frequency/1e9)
            - 0.6 * (setup.user_equipment_height - 1.5))

    return max(nlos, uma_los(setup)[0]), 6.0

# Urban micro street canyon line of sight
def umi_los(setup):
    if setup.distance_2d < 10:
        raise Exception("Not valid less than 10m")
    elif setup.distance_2d > 5e3:
        raise Exception("Not valid over 5km")

    if setup.distance_2d < d_bp(setup):
        return (32.4 + 21.0 * math.log10(setup.distance_3d)
                + 20.0 * math.log10(setup.center_frequency/1e9)), 4.0
    else:
        return (32.4 + 40.0 * math.log10(setup.distance_3d)
                + 20.0 * math.log10(setup.center_frequency/1e9)
                - 9.5 * math.log10(d_bp(setup)**2
                                   + (setup.base_station_height
                                      - setup.user_equipment_height)**2)), 4.0

# Urban micro street canyon non line of sight
def umi_nlos(setup):
    if setup.distance_2d < 10:
        raise Exception("Not valid less than 10m")
    elif setup.distance_2d > 5e3:
        raise Exception("Not valid over 5km")

    nlos = (22.4 + 35.3 * math.log10(setup.distance_3d)
            + 21.3 * math.log10(setup.center_frequency/1e9)
            - 0.3 * (setup.user_equipment_height - 1.5))

    return max(nlos, uma_los(setup)[0]), 7.82
