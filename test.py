import math

import propagation_3gpp

setup = propagation_3gpp.AntennaSetup(
    base_station_height=35,
    user_equipment_height=1.5,
    distance_2d=1000,
    distance_3d=(1000**2+33.5**2)**0.5,
    average_street_width=15,
    center_frequency=2700e6)


print(propagation_3gpp.d_bp(setup))
print(propagation_3gpp.rma_los(setup))
setup.distance_2d = 2000
setup.distance_3d = (2000**2+33.5**2)**0.5
print(propagation_3gpp.rma_los(setup))

setup.distance_2d = 1000
setup.distance_3d = (1000**2+33.5**2)**0.5
print(propagation_3gpp.rma_nlos(setup))
setup.distance_2d = 2000
setup.distance_3d = (2000**2+33.5**2)**0.5
print(propagation_3gpp.rma_nlos(setup))



setup.distance_2d = 1000
setup.distance_3d = (1000**2+33.5**2)**0.5
print(propagation_3gpp.uma_los(setup))
setup.distance_2d = 2000
setup.distance_3d = (2000**2+33.5**2)**0.5
print(propagation_3gpp.uma_los(setup))

setup.distance_2d = 1000
setup.distance_3d = (1000**2+33.5**2)**0.5
print(propagation_3gpp.uma_nlos(setup))
setup.distance_2d = 2000
setup.distance_3d = (2000**2+33.5**2)**0.5
print(propagation_3gpp.uma_nlos(setup))



setup.distance_2d = 1000
setup.distance_3d = (1000**2+33.5**2)**0.5
print(propagation_3gpp.umi_los(setup))
setup.distance_2d = 2000
setup.distance_3d = (2000**2+33.5**2)**0.5
print(propagation_3gpp.umi_los(setup))

setup.distance_2d = 1000
setup.distance_3d = (1000**2+33.5**2)**0.5
print(propagation_3gpp.umi_nlos(setup))
setup.distance_2d = 2000
setup.distance_3d = (2000**2+33.5**2)**0.5
print(propagation_3gpp.umi_nlos(setup))


print("Distance: 500m")
setup.distance_2d = 500
setup.distance_3d = (500**2+33.5**2)**0.5
print(propagation_3gpp.uma_los(setup))

setup.distance_2d = 500
setup.distance_3d = (500**2+33.5**2)**0.5
print(propagation_3gpp.uma_nlos(setup))


print("Distance: 1000m")
setup.distance_2d = 1000
setup.distance_3d = (1000**2+33.5**2)**0.5
print(propagation_3gpp.uma_los(setup))
print(propagation_3gpp.uma_nlos(setup))


print("Distance: 1300m")
setup.distance_2d = 1300
setup.distance_3d = (1300**2+33.5**2)**0.5
print(propagation_3gpp.uma_los(setup))
print(propagation_3gpp.uma_nlos(setup))

import haversine

print(haversine.haversine_distance(40.76793, -111.84559, 40.7677, -111.83814))
